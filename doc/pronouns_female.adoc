// ---
:_gender: woman
:_gender_c: Woman
:_gender_young: girl
:_gender_young_c: Girl
:_gender_young_alt: lass
:_gender_young_alt_c: Lass

:_p: she
:_pp: her
:_po: her
:_pps: hers
:_pc: She
:_ppc: Her
:_ppsc: Hers
:_pr: herself

:_relation-to-parents: daughter
:_relation-to-parents_c: Daughter
:_relation-to-sibling: sister
:_relation-to-sibling_c: Sister

:_attracted-to_young: boy
:_attracted-to: man
:_attracted-to_s: men
:_attracted-to_cs: Men
// ---
