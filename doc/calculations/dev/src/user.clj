(ns user
  (:require [cljc.java-time.local-date :as ld]
            [cljc.java-time.local-time :as lt]
            [camel-snake-kebab.core :as csk]
            [clojure.string :as str]
            [cljc.java-time.format.date-time-formatter :as fmt]
            [cljc.java-time.month :as month]
            [cljc.java-time.period :as pd]
            [invokeawesome.aevum.utils :refer
             [next-occurrence-of-date month-int->str day
              date-pretty time-pretty year-pretty]]))
