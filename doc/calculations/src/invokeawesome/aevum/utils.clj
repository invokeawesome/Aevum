(ns invokeawesome.aevum.utils
  (:require [cljc.java-time.local-date :as ld]
            [cljc.java-time.local-time :as lt]
            [camel-snake-kebab.core :as csk]
            [clojure.string :as str]
            [cljc.java-time.format.date-time-formatter :as fmt]
            [cljc.java-time.month :as month]
            [cljc.java-time.period :as pd]))

(defn day
  [date]
  (ld/format date (fmt/of-pattern "EEEE")))

(defn date-pretty
  [date]
  (ld/format date (fmt/of-pattern "EEEE, MMMM dd, G y")))

(defn year-pretty
  [year]
  (ld/format (ld/of year 1 1) (fmt/of-pattern "y G")))

(defn time-pretty
  [hour minute]
  (lt/format (lt/of hour minute) (fmt/of-pattern "hh:mm a")))

(defn timestamp-pretty
  [date]
  (ld/format date (fmt/of-pattern "EEEE, MMMM dd, yyyy")))

(defn next-occurrence-of-date
  ([starting-date month day-of-month]
   (let [year        (ld/get-year starting-date)
         target-date (ld/of year month day-of-month)]
     (-> (if (ld/is-after starting-date target-date)
           (ld/plus target-date (pd/of-years 1))
           target-date))))
  ([start-date years-after month day-of-month]
   (-> start-date
       (ld/plus-years years-after)
       (next-occurrence-of-date month day-of-month))))

(defn birthday-given-age-at-date
  [date age-at-date birth-month birth-day-of-month]
  (next-occurrence-of-date date
                           (dec (* -1 age-at-date))
                           birth-month birth-day-of-month))

(defn month-int->str [month]
  (-> (month/of month)
      (month/to-string)
      (csk/->PascalCase)))

(defn age-at-date [birth-date date]
  (-> (pd/between birth-date date)
      (pd/get-years)))
