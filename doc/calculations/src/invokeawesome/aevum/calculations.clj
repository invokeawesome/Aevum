(ns invokeawesome.aevum.calculations
  (:require [invokeawesome.aevum.utils :refer
             [next-occurrence-of-date month-int->str day
              date-pretty time-pretty year-pretty
              birthday-given-age-at-date age-at-date]]
            [cljc.java-time.local-date :as ld]
            [cljc.java-time.period :as pd]
            [clojure.string :as str]
            [robertluo.fun-map :refer [fw fun-map]]
            [com.gfredericks.forty-two :refer [words]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Attribute calculations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def calculated-attributes
  {
   ;; Dad birth
   :event_dad_birth_month        8
   :event_dad_birth_day-of-month 19
   :event_dad_birth_date
   (fw {month        :event_dad_birth_month
        day          :event_dad_birth_day-of-month
        mc-birthday  :event_mc_birth_date
        age-mc-birth :event_mc_birth_dad-years-old}
     (birthday-given-age-at-date mc-birthday age-mc-birth month day))
   :event_dad_birth_date-pretty
   (fw {date :event_dad_birth_date}
     (date-pretty date))

   ;; Mom birth
   :event_mom_birth_month        3
   :event_mom_birth_day-of-month 29
   :event_mom_birth_date
   (fw {month        :event_mom_birth_month
        day          :event_mom_birth_day-of-month
        mc-birthday  :event_mc_birth_date
        age-mc-birth :event_mc_birth_mom-years-old}
     (birthday-given-age-at-date mc-birthday age-mc-birth month day))
   :event_mom_birth_year
   (fw {date :event_mom_birth_date}
     (ld/get-year date))
   :event_mom_birth_date-pretty
   (fw {date :event_mom_birth_date}
     (date-pretty date))

   ;; Mom immigration
   :event_mom_immigration_mom-age       14
   :event_mom_immigration_month-of-year 7
   :event_mom_immigration_day-of-month  11
   :event_mom_immigration_date
   (fw {born  :event_mom_birth_date
        age   :event_mom_immigration_mom-age
        month :event_mom_immigration_month-of-year
        day   :event_mom_immigration_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_mom_immigration_year
   (fw {date :event_mom_immigration_date}
     (ld/get-year date))
   :event_mom_immigration_date-pretty
   (fw {date :event_mom_immigration_date}
     (date-pretty date))

   ;; Mom high school graduation
   :event_mom_high-school-grad_mom-age       18
   :event_mom_high-school-grad_month-of-year 6
   :event_mom_high-school-grad_day-of-month  21
   :event_mom_high-school-grad_date
   (fw {born  :event_mom_birth_date
        age   :event_mom_high-school-grad_mom-age
        month :event_mom_high-school-grad_month-of-year
        day   :event_mom_high-school-grad_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_mom_high-school-grad_year
   (fw {date :event_mom_high-school-grad_date}
     (ld/get-year date))

   ;; Mom completes EMT training
   :event_mom_completed-emt-training_total-months 18
   :event_mom_completed-emt-date
   (fw {date   :event_mom_high-school-grad_date
        months :event_mom_completed-emt-training_total-months}
     (ld/plus-months date months))

   ;; Mom and Dad marry
   :event_parents_marriage_mom-age       22
   :event_parents_marriage_month-of-year 5
   :event_parents_marriage_day-of-month  15
   :event_parents_marriage_date
   (fw {born  :event_mom_birth_date
        age   :event_parents_marriage_mom-age
        month :event_parents_marriage_month-of-year
        day   :event_parents_marriage_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_parents_marriage_year
   (fw {date :event_parents_marriage_date}
     (ld/get-year date))

   ;; Protagonist: birth
   :event_mc_birth_mom-years-old 26
   :event_mc_birth_dad-years-old 31
   :event_mc_birth_month         9
   :event_mc_birth_day-of-month  12
   :event_mc_birth_date
   (fw {month      :event_mc_birth_month
        day        :event_mc_birth_day-of-month
        start-date :event_mc_first-vision_date
        start-age  :event_mc_first-vision_mc-years-old}
     (birthday-given-age-at-date start-date start-age month day))
   :event_mc_birth_year
   (fw {date :event_mc_birth_date}
     (ld/get-year date))
   :event_mc_birth_date-pretty
   (fw {date :event_mc_birth_date}
     (date-pretty date))

   ;; Sibling birth
   :event_sib_birth_mc-years-old 4
   :event_sib_birth_month        12
   :event_sib_birth_day-of-month 4
   :event_sib_birth_date
   (fw {month       :event_sib_birth_month
        day         :event_sib_birth_day-of-month
        mc-birthday :event_mc_birth_date
        mc-age      :event_sib_birth_mc-years-old}
     (next-occurrence-of-date mc-birthday mc-age month day))

   ;; Roommate bith
   :event_rm_birth_mc-years-old 1
   :event_rm_birth_month        2
   :event_rm_birth_day-of-month 14
   :event_rm_birth_date
   (fw {month       :event_rm_birth_month
        day         :event_rm_birth_day-of-month
        mc-birthday :event_mc_birth_date
        mc-age      :event_rm_birth_mc-years-old}
     (next-occurrence-of-date mc-birthday mc-age month day))

   :event_creation-of-man_year -99999
   :event_creation-of-man_year-pretty
   (fw {year :event_creation-of-man_year}
     (year-pretty year))

   :event_noahic-flood_year -9999
   :event_noahic-flood_year-pretty
   (fw {year :event_noahic-flood_year}
     (year-pretty year))

   :event_mom_college-graduation-age nil

   ;; Protagonist: First grade
   :event_mc_first-grade_month-of-year 8
   :event_mc_first-grade_day-of-month  30
   :event_mc_first-grade_years         6
   :event_mc_first-grade_date
   (fw {born  :event_mc_birth_date
        age   :event_mc_first-grade_years
        month :event_mc_first-grade_month-of-year
        day   :event_mc_first-grade_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_mc_first-grade_month
   (fw {month :event_mc_first-grade_month-of-year}
     (month-int->str month))

   ;; Protagonist: Reality Acceptance
   :event_mc_reality-acceptance_mc-years-old  17
   :event_mc_reality-acceptance_month-of-year 1
   :event_mc_reality-acceptance_day-of-month  3
   :event_mc_reality-acceptance_date
   (fw {born  :event_mc_birth_date
        age   :event_mc_reality-acceptance_mc-years-old
        month :event_mc_reality-acceptance_month-of-year
        day   :event_mc_reality-acceptance_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_mc_reality-acceptance_grade
   (fw {first-grade-date :event_mc_first-grade_date
        acceptance-date  :event_mc_reality-acceptance_date}
     (-> (pd/between first-grade-date acceptance-date)
         (pd/get-years)
         (+ 1)))
   :event_mc_reality-acceptance_month
   (fw {month :event_mc_reality-acceptance_month-of-year}
     (month-int->str month))

   ;; Protagonist: Trauma
   :event_mc_trauma_mc-years-old  11
   :event_mc_trauma_month-of-year 10
   :event_mc_trauma_day-of-month  04
   :event_mc_trauma_day
   (fw {date :event_mc_trauma_date}
     (day date))
   :event_mc_trauma_date-pretty
   (fw {date :event_mc_trauma_date}
     (date-pretty date))
   :event_mc_trauma_date
   (fw {born  :event_mc_birth_date
        age   :event_mc_trauma_mc-years-old
        month :event_mc_trauma_month-of-year
        day   :event_mc_trauma_day-of-month}
     (next-occurrence-of-date born age month day))
   :event_mc_trauma_sib-years-old
   (fw {born :event_sib_birth_date
        date :event_mc_trauma_date}
     (-> (pd/between born date)
         (pd/get-years)))
   :event_mc_trauma_sib-years-old_words
   (fw {num :event_mc_trauma_sib-years-old}
     (words num))

   :event_mc_trauma_mom-years-old
   (fw {born :event_mom_birth_date
        date :event_mc_trauma_date}
     (age-at-date born date))
   :event_mc_trauma_year
   (fw {date :event_mc_trauma_date}
     (ld/get-year date))

   :event_mc_trauma_times_scene-open_hour   8
   :event_mc_trauma_times_scene-open_minute 12
   :event_mc_trauma_times_scene-open_timestamp
   (fw {hour   :event_mc_trauma_times_scene-open_hour
        minute :event_mc_trauma_times_scene-open_minute}
     (time-pretty hour minute))


   :event_mc_first-vision_year          2014
   :event_mc_first-vision_mc-years-old  22
   :event_mc_first-vision_month-of-year 07
   :event_mc_first-vision_day-of-month  30
   :event_mc_first-vision_date
   (fw {year  :event_mc_first-vision_year
        month :event_mc_first-vision_month-of-year
        day   :event_mc_first-vision_day-of-month}
     (ld/of year month day))
   :event_mc_first-vision_sib-years-old
   (fw {born :event_sib_birth_date
        date :event_mc_first-vision_date}
     (age-at-date born date))
   :event_mc_first-vision_rm-years-old
   (fw {born :event_rm_birth_date
        date :event_mc_first-vision_date}
     (age-at-date born date))
   :event_mc_first-vision_date-pretty
   (fw {date :event_mc_first-vision_date}
     (date-pretty date))

   :event_owg-crossover_days-after-first-vision 1
   :event_owg-crossover_date
   (fw {vision-date :event_mc_first-vision_date
        days-after  :event_owg-crossover_days-after-first-vision}
     (ld/plus-days vision-date days-after))
   :event_owg-crossover_date-pretty
   (fw {date :event_owg-crossover_date}
     (date-pretty date))



   :mc_years-since-trauma
   #_ (fw {start  :mc_journey-start-age
           trauma :mc_trauma-age} (- start trauma))

   :event_mc-trauma-year
   #_ (fw {start       :mc_journey-start-age
           years-since :mc_trauma-age} (- start years-since))})

(defn format-key [k]
  (-> k
      (str/replace #"\:" "")
      (str/replace #"[\.\/]" "_")))

(-> (->> calculated-attributes
         (fun-map)
         (into [])
         (reduce
          (fn [doc [k v]]
            (->> (format ":%s: %s\n"
                         (format-key k) v)
                 (conj doc)))
          [])
         (sort)
         (apply str)
         (spit "calculated-attributes.adoc")))
