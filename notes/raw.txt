Talking points:

Quantum physics -> higher dimensions -> many worlds -> aeviternity
- Their change is our whole timeline changing
- Exerting their presence (recall the "lateness" of Gabriel)
- They don't observe the whole of the timeline at once, nor every possibility, but do have a super-temporal perspective
- The "glassy sea"? "Glossa Ordinaria"
  - Old testament pseudopigraphy: Sea shows up in connection with throne
  - Marginal gloss for 15:1-3:
    AND: "Because among the baptized, the fire of the holy spirit was given to them." "Sea of glass is filled with the multitude of those who are saved" Signifies the splendor that is given to them. Their virtue and strength radiates like the shining sun from within. Works are proved by fire (refer to St. Paul)
  - AND: Refers to a St Andrew (of Caesaria)
    Nicholas DeLyra: What follows from the mention of the glassy sea is the description of the divine phrases which are to follow the persecutions by the reprobate. Also refers to baptism w.r.t the sea. Moral interpretation: sea mixed with fire signifies baptism which is conferred in water, and the Holy Spirit is divine fire. 

Grace's guardian angel
- All people are given guardian angels. The purpose for which each guardian angel is created is to be the guardian of their ward.
- A non-standard "model"
- Initially not self-ware, more like a machine
- Did not distinguish itself from Grace at first
- Creature from Zechariah: a being built for transporting unclean things

Floodless world
- The fallen watchers were prevented by the angels from creating the Nephilim
- What would God's religion look like? Similar progression?
- Opinion of "nephilim" tend to be: "sons of God" and "sons of Man" refers to descendents of Cain and Seth. 
- Strabo: 
- Drome: "Elohim" being plural, "God" or "gods." "Sons of the gods": gods could be understood as saints or angels.
- Symachus: "Sons of the powerful" "Religious sons of Seth" as sons of God being overcome by concupiscence. 
- Crossing things that shouldn't be crossed will yield strange results ("giants")
- Augustine: it could be asked how angels could come together with daughters of men and giants be born from them. But, a great number of Greek and Latin codices don't say "angels" but "sons of God." This solves the question: just men who believed could be called "angels." (John the baptist called an angelos.) They fell because of their concupiscence. Seems more likely than angels who don't have flesh and would not be subject to concupiscence.
- Theodoret: Sons of god = bad angels. But goes on to say "sons of Seth."
- Procopius: Sons of Seth. Only "whole" burned offerings used with Abel's sacrifice. 
- St Victor: Or should we believe that God taught Adam the divine cult in order that he might recover the divine benevolence? It was to him and his sons that he taught that the 10th part and the firstfruits should be given to God. 
- St Jerome: An interpretation given by Ioditsionus rendered the hebrew as "and the Lord was inflamed upon Abel and was not inflamed upon the sacrifice of Cain." By this we should understand that fire came down from heaven and devoured the sacrifice of Abel, just as in the dedication of the temple and with Elijah on Mt Carmel.

Divine cult: whole burnt offerings. Priesthood of the firstborn was what it was supposed to be until the tribe of Levi kills a bunch of evil-doers and gives the priesthood to the levites. Prior to that point, it was firstborn males.

Melchizadech: he was actually Shem?! He was alive when Abraham was alive. The bread and wine offering: something that rose up in the new cult after the flood? Shem would have learned the good stuff from Noah
(Read "How Christ Celebrated the First Mass")
Bread and wine offering of Melchizadech passed down from Adam through Shem?
A prophet: "In the time of the Messiah, the only offering that will remain is the Todah" (thank offering) One Todah is the offering of bread.
Bread and Wine offering is the offering from the beginning of humanity
The Todah is not permitted to be offered on the Sabbath. So Jewish Christians got together as soon as the Sabbath was over (thus the Sunday celebration)
Church's priesthood is also Melchizadechian
The Church used to catagorized male saints as "martyrs or confessors." Confessors were divided between "confesosr bishops and not-bishops." Layman and priest were ranked the same. New categorization (post VII) lumps together priests and bishops. In the prayer texts, the responsories for the divine office for Confessor Bishops = "You are a priest forever according to the order of Melchizadech"
Also repeated at priestly ordinations. 
Christ's priesthood is the reestablishment of the priesthood of the firstborn. "Firstborn of the dead and firstborn of the Father. Also firstborn of the reestablished human race through Mary."

You might see animal holocausts, but the communion sacrifice would be bread and wine.
Incidentally, when the Messiah comes in this world, it makes it much easier to set up the sacraments. "I am the holocaust and I am the bread and wine."

Messiah would still probably be executed.

Sons of Seth possibly would not have killed the Messiah? Messiah as priest-king? Sons of Cain would not have wanted to recognize his authority and power? Cain before God: I'm in terrible straits, they'd kill me! That begets violence upon violence, you get Lamech-type people.
Sons of Cain recognize the "man above men" among the sons of Seth could be the end of them, and they make war on him. Perhaps the God-man goes out and submits himself to be taken and killed by them. Sons of Seth say "don't do that" but "that's how it gots to be"

This war sounds a lot like the apocalypse (think Lion Witch and Wardrobe, how that went down).

What if he was not the priest-king of the sons of Seth? What if he was one of the sons of Cain? You'd need a preparation like how Israel was set up. Israel was a remnant.

What if when God-man priest-king submits himself to death at the hands of the Cainites, and rises from the dead, he tells the Sethites that he did it because the Cainites need to be saved. Here's the new meaning of the sacrifice. I'm leaving you to take care of this. Some Sethites fall. The rest learn that they need to be come one people, just like Jews and Gentiles.

Sethites would need the sacrifice, but wouldn't need it to the same degree as the Cainites. (Think about Joachim and Anna, or St Joseph.) Personal sin is kept more in check. They would still need the sacrifice of the God-man to enter heaven, but wouldn't need it to overcome personal sin. 

Seth view fits: Line from Adam through Seth down to Noah describes the path that the oral tradition followed (the worship of God.) If the descendents of Seth are the sons of God, with the descendents of Cain as the sons of Man, you've got "good humans" and "bad humans." Only a tiny remnant left -> flood.
If you avoided that situation, and concupisence didn't take down the sons of Seth, you could have "two races" (pastoral vs agricultural). Sethians not eating meat? (Book of Jubilees). God gives Noah permission to eat meat.
An essential part of offering sacrifice is partaking of the sacrifice.
Abel offered a lamb. Did he not partake of the sacrifice?
One does not partake of a "holocaust" offering (totally burned off). 

Read "Kolbe Institute" and their lectures on Youtube
12-episode documentary on the development of the theory of evolution, theological problems.

Angels perceive the entire dimensionality of the universe simultaneously
Angels benig "in a place" is to avert their will and intellect to that place / object
Angels vs Demons: things come down to "rights" claimed on the order of nature. Demons hold on to those rights doggedly. Opposition is trying to "unseat" them from that right or "over power" them.
  - Try to figure out what the source of the right is

They don't have automatic access to another creature's mind (including other angels)
In a person, you can't access the abstraction of them without direct access

St Bonaventure:
Angels can be differentiated by spiritual matter. (Tradition has not picked up on this.)

Guardian angels are specifically for the person. Nature is designed for the individual to whom they are assigned.

Salvation or damnation of an angel is based on whether they will obey or not
Guardian angel's salvation or damnation is based on whether they accept the task of guarding them
Since there are some that declined, there must be "double angels" that are both meant for the person. That would mean that some 
